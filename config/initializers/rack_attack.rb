Rack::Attack.cache.store = ActiveSupport::Cache::MemoryStore.new # defaults to Rails.cache
# Always allow requests from localhost (blocklist & throttles are skipped)
Rack::Attack.safelist('allow from localhost') do |req|
  # Requests are allowed if the return value is truthy
  '127.0.0.1' == req.ip || '::1' == req.ip
end
Rack::Attack.throttle("requests by ip", limit: 5, period: 2) do |request|
  request.ip
end
Rack::Attack.throttled_response = lambda do |env|
  retry_after = (env['rack.attack.match_data'] || {})[:period]
  [
    429,
    {'Content-Type' => 'application/json', 'Retry-After' => retry_after.to_s},
    [{error: "Throttle limit reached. Retry later."}.to_json]
  ]
end
