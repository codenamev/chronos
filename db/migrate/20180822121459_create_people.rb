class CreatePeople < ActiveRecord::Migration[5.2]
  def change
    create_table :people do |t|
      t.string :name
      t.string :email, null: false, unique: true
      t.integer :venues_count, default: 0

      t.timestamps
    end
  end
end
