require 'test_helper'

class PersonTest < ActiveSupport::TestCase
  def setup
    @person = build(:person)
  end

  test "valid person" do
    assert @person.valid?
  end

  test "invalid without name" do
    @person.name = nil
    refute @person.valid?, "person is valid without a name"
    assert_not_nil @person.errors[:name], "no validation error for name present"
  end

  test "invalid without email" do
    @person.name = nil
    refute @person.valid?, "person is valid without a email"
    assert_not_nil @person.errors[:email], "no validation error for email present"
  end
end
