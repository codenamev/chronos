require 'test_helper'

class VenueTest < ActiveSupport::TestCase
  def setup
    @venue = build_stubbed(:venue)
  end

  test "valid venue" do
    assert @venue.valid?
  end

  test "invalid without name" do
    @venue.name = nil
    refute @venue.valid?, "venue is valid without a name"
    assert_not_nil @venue.errors[:name], "no validation error for name present"
  end

  test "invalid without owner" do
    @venue.owner_id = nil
    refute @venue.valid?, "venue is valid without an owner"
    assert_not_nil @venue.errors[:owner], "no validation error for owner present"
  end

  test "invalid without street" do
    @venue.street = nil
    refute @venue.valid?, "venue is valid without a street"
    assert_not_nil @venue.errors[:street], "no validation error for street present"
  end

  test "invalid without city" do
    @venue.city = nil
    refute @venue.valid?, "venue is valid without a city"
    assert_not_nil @venue.errors[:city], "no validation error for city present"
  end

  test "invalid without state" do
    @venue.state = nil
    refute @venue.valid?, "venue is valid without a state"
    assert_not_nil @venue.errors[:state], "no validation error for state present"
  end

  test "invalid without zipcode" do
    @venue.zipcode = nil
    refute @venue.valid?, "venue is valid without a zipcode"
    assert_not_nil @venue.errors[:zipcode], "no validation error for zipcode present"
  end

  test "#owner?(person)" do
    owner = build_stubbed(:person, id: @venue.owner_id)
    assert @venue.owner?(owner)
  end

  test "#owner?(person) not owner" do
    owner = build_stubbed(:person, id: @venue.owner_id + 1)
    refute @venue.owner?(owner)
  end

  test "#to_s" do
    assert @venue.to_s, @venue.name
  end

  test "#city_and_state" do
    assert @venue.city_and_state, "#{@venue.city}, #{@venue.state}"
  end

  test "#city_and_state with blank city" do
    @venue.city = ""
    assert @venue.city_and_state, @venue.state
  end

  test "#city_and_state with blank state" do
    @venue.state = ""
    assert @venue.city_and_state, @venue.city
  end

  test "#full_address" do
    assert @venue.full_address, "#{@venue.street}, #{@venue.city}, #{@venue.state} #{@venue.zipcode}"
  end

  test "#google_maps_url" do
    assert @venue.google_maps_url, "http://maps.google.com/?q=#{URI.encode(@venue.name)}&near=#{URI.encode(@venue.full_address)}"
  end
end
