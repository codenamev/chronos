require 'test_helper'

class VenuesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @venue = create(:venue)
  end

  test "should get index" do
    get venues_url, as: :json
    assert_response :success
  end

  test "should create venue" do
    owner = create(:person)
    assert_difference('Venue.count') do
      post venues_url, params: { venue: attributes_for(:venue, owner_id: owner.id) }, as: :json
    end

    assert_response 201
  end

  test "should show venue" do
    get venue_url(@venue), as: :json
    assert_response :success
  end

  test "should update venue" do
    patch venue_url(@venue), params: { venue: attributes_for(:venue) }, as: :json
    assert_response 200
  end

  test "should destroy venue" do
    assert_difference('Venue.count', -1) do
      delete venue_url(@venue), as: :json
    end

    assert_response 204
  end
end
