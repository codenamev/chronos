class VenueSerializer
  include FastJsonapi::ObjectSerializer
  cache_options enabled: true, cache_length: 12.hours

  belongs_to :owner, record_type: :user

  attributes :name, :street, :city, :state, :zipcode, :city_and_state, :google_maps_url
end
