class Person < ApplicationRecord
  has_many :venues, foreign_key: :owner_id, counter_cache: true

  validates :name, :email, presence: true

  def to_s
    name.present? ? name : "New Person"
  end

  def name_and_email
    "#{self.name} <#{self.email}>"
  end
end
