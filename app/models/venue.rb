class Venue < ApplicationRecord
  acts_as_mappable lat_column_name: :latitude, lng_column_name: :longitude

  belongs_to :owner, class_name: "Person"
  delegate :name, :name_and_email, to: :owner, prefix: :owner, allow_nil: true

  validates :name, :street, :city, :state, :zipcode, presence: true

  before_save :geocode_address

  def owner?(user)
    user.present? && owner_id == user.id
  end

  def to_s
    name
  end

  def city_and_state
    [city, state].reject(&:blank?).join(", ")
  end

  def full_address
    [street, city, state].reject(&:blank?).join(", ") + zipcode.to_s
  end

  def google_maps_url
    "http://maps.google.com/?q=#{URI.encode(name)}&near=#{URI.encode(full_address)}"
  end

  private

  def geocode_address
    logger.tagged("geocode-venue-#{id || "new"}") do
      geo = Geokit::Geocoders::MultiGeocoder.geocode(full_address)
      logger.info("Could not geocode address – #{full_address}") if !geo.success
      self.latitude, self.longitude = geo.lat,geo.lng if geo.success
    end
  end
end
